<?php
// Determine if there is a global or page modal turned on.
$page_popup_on   = get_field( 'turn_popup_on' ) === 'on';
$global_popup_on = get_field( 'turn_popup_on', 'options' ) === 'on';
$disable_popup   = get_field( 'disable_global_popup', get_the_ID() ) === 'yes';
$turn_popup_on   = $page_popup_on || ( $global_popup_on && ! $disable_popup );
$pid             = $page_popup_on ? get_the_ID() : 'options';
?>


<?php if ( $turn_popup_on ) : ?>
  <div class="pop-over <?php the_field( 'lightbox_type', $pid ); ?>">
	<?php
	// Fetch modal context (global/page) and output cookie information for
	// JavaScript to consume.
	$frequency   = get_field( 'popup_frequency', $pid );
	$cookie_name = get_field( 'cookie_name', $pid );
	?>
	<div
	  class="js-popup-options"
	  data-frequency="<?php echo $frequency; ?>"
	  data-cookie-name="we_c_<?php echo $cookie_name; ?>"
	>
	</div>

	<?php
	// Image modal.
	// --------------------------------------------------------------------------
	?>
	<?php
	if (
	  'image' == get_field( 'lightbox_type', $pid ) &&
	  get_field( 'popup_image', $pid )
	) :
		?>
		<?php if ( ! get_field( 'image_url', $pid ) ) : ?>
		<a
		  id="image-popup"
		  class="hide-me"
		  href="<?php echo get_field( 'popup_image', $pid ); ?>"
		>
			<?php esc_attr_e( 'Splash Image', 'we_modal_plugin' ); ?>
		</a>
		<?php else : ?>
		<a id="image-popup-linkable" class="hide-me" href="#image-popup-inline">
			<?php esc_attr_e( 'Splash Image', 'wectheme' ); ?>
		</a>
		<div id="image-popup-inline" class="mfp-hide">
		  <div class="image-wrap">
			<button
			  title="<?php esc_attr_e( 'Close (Esc)', 'we_modal_plugin' ); ?>"
			  type="button"
			  class="mfp-close"
			>
			  &times;
			</button>
			  <a
				href="<?php echo get_field( 'image_url', $pid ); ?>"
				target="_blank"
			  >
				<img
				  src="<?php echo get_field( 'popup_image', $pid ); ?>"
				  alt=""
				/>
			  </a>
		  </div>
		</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php
	// Video modal.
	// --------------------------------------------------------------------------
	?>
	<?php if ( 'video' == get_field( 'lightbox_type', $pid ) ) : ?>
	  <div
		class="play-row"
		<?php if ( get_field( 'autoplay_video', $pid ) == 'yes' ) : ?>
		  data-autoplay="true"
		<?php endif; ?>
	  >
		<a data-fancybox href="<?php the_field( 'popup_video_id', $pid ); ?>"
		  <?php if ( get_field( 'autoplay_video', $pid ) == 'yes' ) : ?>
			data-autoplay="true"
			<?php endif; ?>
		  >
		  <div class="play-button">
			<div class="play-bg video-play-bg">
			  <div class="video-play-bg-inner">
			  <?php
				if ( get_field( 'video_cover_image', 'options' ) && get_field( 'autoplay_video', $pid ) != 'yes' ) :
					$videoImage = get_field( 'video_cover_image', 'options' );
					$size       = 'topper-image';
					$videoThumb = $videoImage['sizes'][ $size ];
					?>
				<img
				  data-src="<?php echo $videoThumb; ?>"
				  style="width: 100%;"
				  alt=""
				/>
					<?php endif; ?>
			  </div>
			</div>
			<?php if ( get_field( 'autoplay_video', 'options' ) == 'no' ) : ?>
			  <div class="modal-play-button"><?php get_template_part( 'partials/icons/icon', 'play' ); ?></div>
			<?php endif; ?>
		  </div>
		</a>
	  </div>
	<?php endif; ?>

	<?php
	// HTML modal.
	// --------------------------------------------------------------------------
	?>
	<?php if ( 'html' == get_field( 'lightbox_type', $pid ) ) : ?>
	  <a id="inline-popup-link" class="hide-me" href="#inline-popup">
		<?php esc_attr_e( 'Splash Statement', 'we_modal_plugin' ); ?>
	  </a>
	  <div id="inline-popup" class="mfp-hide">
		<div class="inline-wrap">
		  <button title="<?php esc_attr_e( 'Close (Esc)', 'we_modal_plugin' ); ?>" type="button" class="mfp-close">
			&times;
		  </button>
		  <?php echo get_field( 'inline_popup', $pid ); ?>
		</div>
	  </div>
	<?php endif; ?>
  </div><!-- .pop-over -->
<?php endif; ?>
