<?php
/*
Plugin Name: a11y Modal Dialog
Description: Enables an ACF-based solution for simple Modal dialogs
Version: 1.5
Author: WideEye
Author URI: https://www.wideeye.co
*/

/*  Copyright 2009 David Gwyer (email : david@wpgoplugins.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_action( 'init', 'we_a11y_enable_modal_plugin' );

function we_a11y_enable_modal_plugin(){
    //var_dump(get_current_screen()); exit();
    if( function_exists( 'acf_add_local_field_group' ) && function_exists( 'acf_add_options_page' ) ) {
        acf_add_options_page(
            array(
                'page_title' => __( 'Modal Popup', 'we_modal_plugin' ),
                'menu_title' => __( 'Modal Popup', 'we_modal_plugin' ),
                'menu_slug'  => 'modal-popup',
            )
        );
        require_once('acf/modal-popup-acf.php');
        add_action(
            'admin_head',
            function() {
                echo '<style>.toplevel_page_modal-popup [data-name="disable_global_popup"], body:not(.toplevel_page_modal-popup) [data-name="show_modal_on_pages"]{display:none}</style>';
            }
        );
    }
    else{
        return null;
    }

    add_action( 'template_redirect', 'we_a11y_init_modal_frontend' );
}

function we_a11y_init_modal_frontend(){
    $plugin_dir = plugin_dir_url( __FILE__ );
    
    $pages_enabled = 'yes' === get_option( 'options_show_modal_on_pages', false );
    $page_popup_on   = $pages_enabled && 'on' === get_post_meta( get_the_ID(), 'turn_popup_on', true );
    
    $global_popup_on = 'on' === get_option( 'options_turn_popup_on', false );
    $disable_popup   = 'yes' === get_post_meta( get_the_ID(), 'disable_global_popup', true );
    $turn_popup_on   = $page_popup_on || ( $global_popup_on && ( ! $pages_enabled || ! $disable_popup ) );

    if ( $turn_popup_on ) :
        $pid = $page_popup_on ? get_the_ID() : 'options';

        wp_enqueue_script(
          'we_a11y-dialog_js',
          $plugin_dir  . 'js/a11y-dialog.min.js',
          array('vendors_js_defer'),
          'v1',
          true
        );

        add_action('wp_footer', 'we_a11y_display_a11y_dialog');

    endif;
}


function we_a11y_enhance_oembed_iframe( $iframe, $autoplay_attempt = 0 ) {
    preg_match( '/src="(.+?)"/', $iframe, $matches );
    if( ! array_key_exists( "1", $matches ) ){
        return $iframe;
    }
    $src = $matches[1];

    // Add extra parameters to src and replcae HTML.
    $params = array(
        'controls'  => 1,
        'hd'        => 1,
        'autohide'  => 1,
        'showinfo'  => 1,
        'feature'   => 'oembed',
        'rel'       => 0,
        'autoplay'  => $autoplay_attempt,
        'wmode'     => 'transparent',
        'html5'     => 1
    );
    $new_src = add_query_arg( $params, $src );
    $iframe = str_replace( $src, $new_src, $iframe );

    // Add extra attributes to iframe HTML.
    $attributes = 'frameborder="0" allowfullscreen scrolling="no"';
    $iframe = str_replace( '></iframe>', ' ' . $attributes . '></iframe>', $iframe );
    return str_replace( 'allow="', 'allow="fullscreen; ' . ( $autoplay_attempt ? 'autoplay; ' : '' ), $iframe );
}

function we_a11y_custom_wpkses_post_tags( $tags, $context ) {

    $tags['iframe'] = array(
        'class'           => true,
        'src'             => true,
        'height'          => true,
        'allow'           => true,
        'width'           => true,
        'scrolling'       => true,
        'frameborder'     => true,
        'allowfullscreen' => true,
        'title'           => true,
    );

    return $tags;
}

add_filter( 'wp_kses_allowed_html', 'we_a11y_custom_wpkses_post_tags', 10, 2 );

function we_a11y_title_type( $type ) {
    switch ($type) {
        case 'video':
            echo "a video.";
            break;
        case 'image':
            echo "an image.";
            break;
        default:
           echo "textual content.";
    }
}

function we_a11y_display_a11y_dialog(){
    $pages_enabled = 'yes' === get_option( 'options_show_modal_on_pages', false );
    $page_popup_on   = $pages_enabled && 'on' === get_post_meta( get_the_ID(), 'turn_popup_on', true );
    $pid = $page_popup_on ? get_the_ID() : 'options';

    if( ! defined('DISALLOW_COOKIES') || true !== constant('DISALLOW_COOKIES') ){
        $frequency   = get_field( 'popup_frequency', $pid );
        $cookie_name = get_field( 'cookie_name', $pid );
    }

    $lightbox_type = get_field( 'lightbox_type', $pid );

    ?>
        <div class="dialog dialog-type-<?php echo esc_attr( $lightbox_type ); ?>" id="accessible-dialog"
        <?php 
            if ( ! defined('DISALLOW_COOKIES') || true !== constant('DISALLOW_COOKIES') ) :
            ?>   
            data-frequency="<?php echo esc_attr( $frequency ); ?>"
            data-cookie-name="we_c_<?php echo esc_attr( $cookie_name ); ?>"
        <?php endif; ?>
        aria-hidden="true">
            <div class="overlay dialog-overlay" tabindex="-1" data-a11y-dialog-hide></div>
            <div class="dialog-content" role="alertdialog" aria-labelledby="dialog-title" aria-modal="true">
                <button class="dialog-close" type="button" data-a11y-dialog-hide aria-label="Close this dialog window">
                  &times;
                </button>
                <h1 id="dialog-title" class="dialog-title screen-reader-text">A modal dialog containing <?php echo esc_html( we_a11y_title_type( $lightbox_type ) ); ?> (Press escape to close)</h1>
                <div class="dialog__scroll_wrap">
                    <div class="dialog-body">
                        <?php if ( 'image' === $lightbox_type ) :
                            $image_url = get_field( 'image_url', $pid );
                            $image = get_field( 'popup_image', $pid );
                            if ( $image_url ) : 
                                $image_url_href = $image_url['url'];
                                $image_url_title = $image_url['title'];
                                $image_url_target = $image_url['target'] ? $image_url['target'] : '_self';
                                ?>
                              <a
                                href="<?php echo esc_url( $image_url_href ); ?>"
                                target="<?php echo esc_attr( $image_url_target ); ?>"
                                class="overlay-link"
                                title="<?php echo esc_attr( $image_url_title ); ?>"
                              ><span class="screen-reader-text"><?php echo esc_attr( $image_url_title ); ?></span></a>
                            <?php endif; ?>

                            <?php 
                            echo wp_get_attachment_image( $image['ID'], array('700', '600'), "", array( "class" => "img-responsive" ) );
                            ?>
                        <?php endif; ?>
                        <?php if ( 'video' === $lightbox_type ) :
                            $iframe = get_field( 'popup_video', $pid );
                            $autoplay_attempt = "yes" === get_field( 'autoplay_video', $pid )  ? 1 : 0;
                            ?>
                            <div class="responsive-embed responsive-embed-16by9">
                                <?php
                                echo wp_kses_post( we_a11y_enhance_oembed_iframe( $iframe, $autoplay_attempt ) );
                                ?>
                            </div>
                            <?php endif; ?>
                        <?php if ( 'html' === $lightbox_type ) : ?>
                            <div class="wysiwyg-text">
                                <?php 
                                echo wp_kses_post( get_field( 'popup_inline_content', $pid ) );
                                ?>
                            </div>
                            <?php if ( have_rows( 'add_popup_action_buttons', $pid ) ) : ?>
                                <div class="row justify-content-around buttons__row">
                                    <?php
                                    while ( have_rows( 'add_popup_action_buttons', $pid ) ) :
                                        the_row();
                                        $link = get_sub_field( 'button_link' );
                                        ?>
                                        <div class="col col-lg-auto">
                                          <a
                                            class="btn btn--tertiary"
                                            href="<?php echo esc_url( $link['url'] ); ?>"
                                            <?php if ( $link['target'] ) : ?>
                                                  target="_blank"
                                            <?php endif; ?>
                                              >
                                            <?php echo esc_html( $link['title'] ); ?>
                                          </a>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
};
