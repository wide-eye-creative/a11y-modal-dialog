<?php

function we_add_modal_acf_fields() {

  $show_on_pages = "yes" === get_option( 'options_show_modal_on_pages' );

  $location_array = array(
    array(
      array(
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'modal-popup',
      ),
    )
  );

  if ( $show_on_pages ) {
    $location_array[] = array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
      ),
      array(
        'param' => 'page_type',
        'operator' => '!=',
        'value' => 'front_page',
      ),
    );
  }

  acf_add_local_field_group(array(
    'key' => 'group_576b74c898b56',
    'title' => 'Modal Popup',
    'location' => $location_array,
    'position' => 'acf_after_title',
    'menu_order' => 5,
    'wpml_cf_preferences' => '0',
    'hide_on_screen' => array(
      0 => 'the_content',
    ),
  ));

  acf_add_local_field(array(
    'key' => 'field_576b7585f5fd0',
    'label' => 'Enable Popup',
    'name' => 'turn_popup_on',
    'type' => 'button_group',
    'instructions' => 'Turn on the popup dialog.',
    //'instructions' => 'Show a popup dialog' . ( $show_on_pages &&  ? ' on this page' : ' sitewide.' ),
    'wrapper' => array(
      'width' => '30',
    ),
    'wpml_cf_preferences' => '0',
    'choices' => array(
      'off' => 'Off',
      'on' => 'On',
    ),
    'default_value' => 'off',
    'layout' => 'horizontal',
    'return_format' => 'value',
    'parent' => 'group_576b74c898b56',
  ));

  acf_add_local_field(array(
    'key' => 'field_876h7585f5fd0_cf',
    'label' => 'Show Override Modal Controls on Pages?',
    'name' => 'show_modal_on_pages',
    'instructions' => 'Enable if you want to be able to override the global modal with ones specific to other pages.',
    'type' => 'button_group',
    'choices' => array(
      'no' => 'No',
      'yes' => 'Yes',
    ),
    'wpml_cf_preferences' => '0',
    'default_value' => 'no',
    'wrapper' => array(
      'width' => '70',
    ),
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'layout' => 'horizontal',
    'return_format' => 'value',
    'parent' => 'group_576b74c898b56',
  ));

  acf_add_local_field(array(
    'key' => 'field_5c09db650f09f',
    'label' => 'Disable Global Popup',
    'name' => 'disable_global_popup',
    'type' => 'button_group',
    'instructions' => 'Prevent the global site popup from appearing on just this page.',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'off',
        ),
      ),
    ),
    'wrapper' => array(
      'width' => '33',
    ),
    'wpml_cf_preferences' => '0',
    'choices' => array(
      'no' => 'No',
      'yes' => 'Yes',
    ),
    'default_value' => 'off',
    'layout' => 'horizontal',
    'return_format' => 'value',
    'parent' => 'group_576b74c898b56',
  ));

  // if sites don't allow cookies, this is all irreeeeeelevant
  if( ! defined('DISALLOW_COOKIES') || ! constant('DISALLOW_COOKIES') ){

      acf_add_local_field(array(
        'key' => 'field_5a98dc07ba6f3',
        'label' => 'Frequency',
        'name' => 'popup_frequency',
        'type' => 'button_group',
        'instructions' => 'Set the frequency that the popup will appear to a given user.',
        'required' => 1,
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_576b7585f5fd0',
              'operator' => '==',
              'value' => 'on',
            ),
          ),
        ),
        'wrapper' => array(
          'width' => '70',
        ),
        'wpml_cf_preferences' => '0',
        'choices' => array(
          'always' => 'Always',
          'session' => 'Once per Session',
          'week' => 'Once a Week',
          'once' => 'Once Ever',
        ),
        'default_value' => 'session',
        'layout' => 'horizontal',
        'return_format' => 'value',
        'parent' => 'group_576b74c898b56',
      ));

      acf_add_local_field(array(
        'key' => 'field_576b75e8f5fd2',
        'label' => 'Cookie Name (optional)',
        'name' => 'cookie_name',
        'type' => 'text',
        'placeholder'=> 'lb_cookie1',
        'default_value' => 'lb_cookie1',
        'instructions' => 'Cookie used to determine how often to show the popup. Changing the cookie name will reset the popup for all users. Must all be one word with or without underscores. For example: cookie1, cookie2, issue_cookie_3.',
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_576b7585f5fd0',
              'operator' => '==',
              'value' => 'on',
            ),
            array(
              'field' => 'field_5a98dc07ba6f3',
              'operator' => '!=',
              'value' => 'always',
            ),
          ),
        ),
        'wpml_cf_preferences' => '0',
        'parent' => 'group_576b74c898b56',
      ));
  }

  acf_add_local_field(array(
    'key' => 'field_576b7622f5fd3',
    'label' => 'Lightbox Type',
    'name' => 'lightbox_type',
    'type' => 'select',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'wrapper' => array(
      'width' => '70',
    ),
    'wpml_cf_preferences' => '0',
    'choices' => array(
      'image' => 'Image',
      'video' => 'Video',
      'html' => 'HTML',
    ),
    'required' => 1,
    'default_value' => array(
      0 => 'video',
    ),
    'return_format' => 'value',
    'parent' => 'group_576b74c898b56',
  ));

  acf_add_local_field(array(
    'key' => 'field_576b7650f5fd4',
    'label' => 'Popup Image',
    'name' => 'popup_image',
    'type' => 'image',
    'instructions' => 'Images should be a minimum of 600px wide by 480px tall with as small a filesize as possible.',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'image',
        ),
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'wpml_cf_preferences' => '0',
    'required' => 1,
    'min_width' => 600,
    'min_height' => 480,
    'min_size' => '',
    'max_width' => '',
    'max_height' => '',
    'max_size' => '1.2',
    'return_format' => 'object',
    'preview_size' => 'thumbnail',
    'library' => 'all',
    'parent' => 'group_576b74c898b56',
  ));


  acf_add_local_field(array(
    'key' => 'field_576b767df5fd6',
    'label' => 'Image URL',
    'name' => 'image_url',
    'instructions' => 'Where the image should link to.',
    'type' => 'link',
    'wpml_cf_preferences' => '0',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'image',
        ),
      ),
    ),
    'parent' => 'group_576b74c898b56',
  ));

  // deprecated
  acf_add_local_field(array(
    'key' => 'field_576b76b2f5fd8',
    'label' => 'Play Video Immediately?',
    'name' => 'autoplay_video',
    'type' => 'button_group',
    'instructions' => 'If "Yes," the video will attempt to autoplay (some users/browser may disable/prevent this).',
    'wpml_cf_preferences' => '0',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'video',
        ),
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'wrapper' => array(
      'width' => '30',
    ),
    'choices' => array(
      'no' => 'No',
      'yes' => 'Yes',
    ),
    'layout' => 'horizontal',
    'return_format' => 'value',
    'parent' => 'group_576b74c898b56',
  ));

  acf_add_local_field(array(
    'key' => 'field_576b7691f5fd7',
    'label' => 'Popup Video',
    'name' => 'popup_video',
    'type' => 'oembed',
    'required' => 1,
    'wpml_cf_preferences' => '0',
    'instructions' => 'Here you can paste the video URL from YouTube or other platforms.',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'video',
        ),
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'parent' => 'group_576b74c898b56',
  ));

  acf_add_local_field(array(
    'key' => 'field_5g602f0ec5',
    'label' => 'Video Caption',
    'name' => 'video_caption',
    'type' => 'wysiwyg',
    'instructions' => 'Include a descriptive caption regarding the video content.',
    'required' => 1,
    'conditional_logic' => 0,
    'wrapper' => array(
      'width' => '',
      'class' => '',
      'id' => '',
    ),
    'wpml_cf_preferences' => '0',
    'default_value' => '',
    'tabs' => 'visual',
    'toolbar' => 'minimal',
    'media_upload' => 0,
    'delay' => 0,
    'maxlength' => 260,
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'video',
        ),
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'parent' => 'group_576b74c898b56',
  ));


  acf_add_local_field(array(
    'key' => 'field_576b76cdf5fd9',
    'label' => 'Inline Content',
    'name' => 'popup_inline_content',
    'type' => 'wysiwyg',
    'required' => 1,
    'instructions' => 'Use this if you want more control over the lightbox content. (Advanced)',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'html',
        ),
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'wpml_cf_preferences' => '0',
    'tabs' => 'all',
    'toolbar' => 'full',
    'media_upload' => 1,
    'parent' => 'group_576b74c898b56',
  ));

  acf_add_local_field(array(
    'key' => 'field_58fc4hc59657',
    'label' => 'Action Buttons',
    'instructions' => 'Optional calls to action that will appear below the inline content.',
    'name' => 'add_popup_action_buttons',
    'type' => 'repeater',
    'required' => 0,
    'min' => 1,
    'max' => 2,
    'layout' => 'block',
    'wpml_cf_preferences' => '0',
    'button_label' => 'Add Button',
    'sub_fields' => array(
      array(
        'key' => 'field_587fc516596e8',
        'label' => 'Button',
        'name' => 'button_link',
        'type' => 'link',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => 'ctl-button',
          'id' => '',
        ),
        'wpml_cf_preferences' => '0',
        'return_format' => 'array',
      ),
    ),
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_576b7622f5fd3',
          'operator' => '==',
          'value' => 'html',
        ),
        array(
          'field' => 'field_576b7585f5fd0',
          'operator' => '==',
          'value' => 'on',
        ),
      ),
    ),
    'parent' => 'group_576b74c898b56',
  ));


}
we_add_modal_acf_fields();
